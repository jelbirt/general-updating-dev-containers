![Thea's Pantry](TheasPantry.png)

# Thea's Pantry

[Thea's Pantry](https://www.worcester.edu/Theas-Pantry/) &mdash; A food pantry for the Worcester State University community.

## This `General` project isused for general issue for Thea's Pantry..

**Note:** Thea's Pantry uses `main` for the default branch, rather than `master`.

Licenses &mdash; We license all our code under [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) and all other content under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

---
Copyright &copy; 2021 The LibreFoodPantry Authors. This work is licensed
under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/4.0/.